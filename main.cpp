///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 06a - Animal Farm 3
///
/// @file main.cpp
/// @version 1.0
///
/// Exports data about all animals
///
/// @author Micah Chinen <micah42@hawaii.edu>
/// @brief  Lab 06a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   25_MAR_2021
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <array>
#include <list>

#include "animal.hpp"
#include "animalfactory.hpp"
#include "cat.hpp"
#include "dog.hpp"
#include "nunu.hpp"
#include "aku.hpp"
#include "palila.hpp"
#include "nene.hpp"

using namespace std;
using namespace animalfarm;

int main(){
   cout << "Welcome to Animal Farm 3" << endl;

   //Array Containers
   array<Animal* , 30> animalArray;
   animalArray.fill(NULL);
   
   for(int i =0; i < 25; i++) {
      animalArray[i] = AnimalFactory::getRandomAnimal();
   }

   cout << endl << "Array of Animals:" << endl;
   cout << "   Is it empty: " << boolalpha << animalArray.empty() << endl;
   cout << "   Number of elements: " << animalArray.size() << endl;
   cout << "   Max size: " << animalArray.max_size() << endl;

   for (Animal* animal : animalArray) {
      if(animal != NULL) {
         cout << animal->speak() << endl;
      }
   }

   //Cleanup
   for(int i =0; i < 25; i++) { 
      delete animalArray[i];
      animalArray[i] = NULL;
   }


   //List Containers
   list<Animal*> animalList;
   
   for(int i = 0; i < 25; i++) {
      animalList.push_front(AnimalFactory::getRandomAnimal());
   }

   cout << endl << "List of Animals:" << endl;
   cout << "   Is it empty: " << boolalpha << animalList.empty() << endl;
   cout << "   Number of elements: " << animalList.size() << endl;
   cout << "   Max size: " << animalList.max_size() << endl;

   for (Animal* animal : animalList) {
      if(animal != NULL) {
         cout << animal->speak() << endl;
      }
   }
   //Cleanup
   for (Animal* animal: animalList) {
      delete animal;
   }
   animalList.clear();
   cout << '$' << endl;
   
   return 0;
}


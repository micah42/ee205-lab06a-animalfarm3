///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file animal.cpp
/// @version 1.0
///
/// Exports data about all animals
///
/// @author Micah Chinen <micah42@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   17_FEB_2021
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <string>
#include <stdlib.h>

#include "animal.hpp"

using namespace std;

namespace animalfarm {

Animal:: Animal(){
   cout << ".";
}

Animal:: ~Animal(){
   cout << "x";
}

void Animal::printInfo() {
	cout << "   Species = [" << species << "]" << endl;
	cout << "   Gender = [" << genderName( gender ) << "]" << endl;
}


string Animal::genderName (enum Gender gender) {
   switch (gender) {
      case MALE:    return string("Male");    break;
      case FEMALE:  return string("Female");  break;
      case UNKNOWN: return string("Unknown"); break;
   }

   return string("Really, really Unknown");
};

const enum Gender Animal::getRandomGender() {

   int gender = rand() % 2;

      switch(gender) {
         case 0: return MALE;   break;
         case 1: return FEMALE; break;
      }
      return MALE;
}


string Animal::colorName (enum Color color) {
   
   switch(color){
      case BLACK:  return string("Black");  break;
      case WHITE:  return string("White");  break;
      case RED:    return string("Red");    break;
      case SILVER: return string("Silver"); break;
      case YELLOW: return string("Yellow"); break;
      case BROWN:  return string("Brown");  break;

   }
   return string("Unknown");
};

const enum Color Animal::getRandomColor() {
 
   int color = rand() % 6;

      switch(color) {
         case 0: return BLACK;   break;
         case 1: return WHITE;   break;
         case 2: return RED;     break;
         case 3: return SILVER;  break;
         case 4: return YELLOW;  break;
         case 5: return BROWN;   break;

      }
      return BLACK;
}

const bool Animal::getRandomBool() {
      
   int i = rand() % 2;
      
      switch(i) {
         case 0: return true;  break;
         case 1: return false; break;

      }
      return 0; 
}

const float Animal:: getRandomWeight(const float from, const float to) {
      
   int  range = (int) (to - from);
   float randomWeight = from + (rand()%range);
      
   return randomWeight;
}

const string Animal::getRandomName() {
      
   int length = 4 + (rand() % 6);
   char randomName[length];
  
   randomName[0] = (char) (65 + rand() % 26); 
   
      for (int i = 1; i < length; i++) {
      
         randomName[i] = (char) (97 + rand() % 26);
      }

      return randomName;
}


} // namespace animalfarm

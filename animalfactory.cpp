///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 06a - Animal Farm 3
///
/// @file animalfactory.cpp
/// @version 1.0
///
/// Generates random animal
///
/// @author Micah Chinen <micah42@hawaii.edu>
/// @brief  Lab 06a - AnimalFarm3 - EE 205 - Spr 2021
/// @date   25_MAR_2021
///////////////////////////////////////////////////////////////////////////////

#include <string>
#include <iostream>
#include <stdlib.h>

#include "animal.hpp"
#include "animalfactory.hpp"
#include "cat.hpp"
#include "dog.hpp"
#include "nunu.hpp"
#include "aku.hpp"
#include "palila.hpp"
#include "nene.hpp"

using namespace std;

namespace animalfarm {

   Animal* AnimalFactory::getRandomAnimal(){
      Animal* newAnimal;
     
      // This gives you a number from 0 to 5
      int i = rand() % 6;
      
      switch(i){
         case 0: newAnimal = new Cat      (Animal::getRandomName(), Animal::getRandomColor(), Animal::getRandomGender()); break;
         case 1: newAnimal = new Dog      (Animal::getRandomName(), Animal::getRandomColor(), Animal::getRandomGender()); break;
         case 2: newAnimal = new Nunu     (Animal::getRandomBool(), RED, Animal::getRandomGender()); break;
         case 3: newAnimal = new Aku      (Animal::getRandomWeight(9.2, 20.5), SILVER, Animal::getRandomGender()); break;
         case 4: newAnimal = new Palila   (Animal::getRandomName(), YELLOW, Animal::getRandomGender()); break;
         case 5: newAnimal = new Nene     (Animal::getRandomName(), BROWN, Animal::getRandomGender()); break;
         
         return newAnimal;
      }
      return newAnimal;
   }

} // namespace animalfarm


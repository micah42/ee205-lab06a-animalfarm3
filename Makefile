###############################################################################
# University of Hawaii, College of Engineering
# EE 205  - Object Oriented Programming
# Lab 06a - Animal Farm 3
#
# @file    Makefile
# @version 1.0
#
# @author Micah Chinen <micah42@hawaii.edu>
# @brief  Lab 06a - Animal Farm 3 - EE 205 - Spr 2021
# @date   25_MAR_2021
###############################################################################

all: main

main.o:  animal.hpp animalfactory.hpp main.cpp
	g++ -c main.cpp

animal.o: animal.hpp animal.cpp
	g++ -c animal.cpp
	
mammal.o: mammal.hpp mammal.cpp animal.hpp
	g++ -c mammal.cpp

fish.o: fish.hpp fish.cpp animal.hpp
	g++ -c fish.cpp

bird.o: bird.hpp bird.cpp animal.hpp
	g++ -c bird.cpp

cat.o: cat.cpp cat.hpp mammal.hpp
	g++ -c cat.cpp

dog.o: dog.cpp dog.hpp mammal.hpp
	g++ -c dog.cpp

nunu.o: nunu.cpp nunu.hpp fish.hpp
	g++ -c nunu.cpp

aku.o: aku.cpp aku.hpp fish.hpp
	g++ -c aku.cpp

palila.o: palila.cpp palila.hpp bird.hpp
	g++ -c palila.cpp

nene.o: nene.cpp nene.hpp bird.hpp
	g++ -c nene.cpp

animalfactory.o: animalfactory.cpp animalfactory.hpp
	g++ -c animalfactory.cpp

random.o: animal.hpp animal.cpp animalfactory.cpp animalfactory.hpp
	g++ -c random.cpp

random: random.cpp random.o animal.o animalfactory.o mammal.o fish.o bird.o cat.o dog.o nunu.o aku.o palila.o nene.o
	g++ -o random random.o animal.o animalfactory.o mammal.o fish.o bird.o cat.o dog.o nunu.o aku.o palila.o nene.o

main: main.cpp *.hpp main.o animal.o animalfactory.o mammal.o fish.o bird.o cat.o dog.o nunu.o aku.o palila.o nene.o
	g++ -o main main.o animal.o animalfactory.o mammal.o  fish.o bird.o cat.o dog.o nunu.o aku.o palila.o nene.o
	
clean:
	rm -f *.o main

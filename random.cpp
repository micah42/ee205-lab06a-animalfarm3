///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 06a - Animal Farm 3
///
/// @file random.cpp
/// @version 1.0
///
/// Test file for random attribute generator
///
/// @author Micah Chinen <micah42@hawaii.edu>
/// @brief  Lab 06a - AnimalFarm3 - EE 205 - Spr 2021
/// @date   10_MAR_2021
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <string>

#include "animal.hpp"

using namespace std;
using namespace animalfarm;

int main() {

   srand (time(NULL));

   cout << "Random Attribute Generator:" << endl;
   cout << "   Gender: " << Animal::getRandomGender() << endl;
   cout << "   Color: " << Animal::getRandomColor() << endl;
   cout << "   Bool: " << std::boolalpha << Animal::getRandomBool() << endl;
   cout << "   Weight: " << Animal::getRandomWeight(9.2, 20.5) << endl;
   cout << "   Name: " << Animal::getRandomName() << endl;

}
